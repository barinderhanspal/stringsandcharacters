import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;



public class StringUtil2 {

	public static boolean validateParanthesis(String input){
		java.util.Stack<Character> stack = new java.util.Stack<Character>();
		
		for(int index = 0; index < input.length(); index ++){
			char inputChar = input.charAt(index);
			if(inputChar == '{' || inputChar == '(' || inputChar == '['){
				stack.push(inputChar);
			} else if(inputChar == ')' || inputChar == ']' || inputChar == '}'){
				if(stack.isEmpty() || !areMatchingParanthesis(inputChar, stack.peek())){
					return false;
				} else {
					stack.pop();
				}
			}
		}
		
		return stack.isEmpty();
	}

	private static boolean areMatchingParanthesis(char inputChar, Character peek) {
		switch(inputChar){
		case ')' : return (peek == '(');
		case ']' : return (peek == '[');
		case '}' : return (peek == '{');
		default: return false;
		}
	}
	
	public static boolean isPalindrome(String input){
		int startIndex = 0, endIndex = input.length() - 1;
		
		while(startIndex < endIndex){
			if(input.charAt(startIndex) != input.charAt(endIndex)){
				return false;
			}
			startIndex ++;
			endIndex --;
		}
		return true;
	}
	
	public static int evaluatePostFix(String input){
		java.util.Stack<Integer> stack = new java.util.Stack<Integer>();
		for(int index = 0; index < input.length(); index ++){
			char ch = input.charAt(index);
			if(Character.isDigit(ch)){
				stack.push(Character.getNumericValue(ch));
			} else if (ch == '^' || ch == '*' || ch == '+' || ch == '-' || ch == '/'){
				if(stack.size() >= 2){
					int op2 = stack.pop();
					int op1 = stack.pop();
					stack.push(calculateExpression(op1, ch, op2));
				} else {
					System.out.println("invalid expression");
					return -1;
					
				}
			}
		}
		
		return stack.pop();
	}
	
	public static String printExcelColumnNo(int number){
		String result = "";
		
		while(number > 0){
			int rem = number % 26;
			
			if(rem == 0){
				result += 'Z';
				number = (number/26) -1;
			} else {
				result += (rem - 1) + 'A';
				number = number / 26;
			}
		}
		
		return result;
	}
	
	private static Integer calculateExpression(int op1, char ch, int op2) {
		switch (ch){
		case '^' : return op1^op2;
		case '*' : return op1*op2;
		case '/' : return op1/op2;
		case '-' : return op1 - op2;
		case '+' : return op1 + op2;
		default:
			return -11111111;
		}
	}
	
	public static Set<Entry<String, Integer>> getWordCount(String input){
		String s[] = input.split(" ");
		HashMap<String, Integer> map = new HashMap<String, Integer>();
		for(int index = 0; index < s.length; index ++){
			
			Integer count = map.get(s[index]);
			count = (count == null)? 1 : count + 1;
			map.put(s[index], count);
			/*if(map.containsKey(s[index])){
				map.put(s[index], map.get(s[index]) + 1);
			} else {
				map.put(s[index], 1);
			}*/
		}
		
		return map.entrySet();
	}

	public static boolean isPalindromes(String input){
		if(input == null || input.equals(""))
			return true;
		
		int s = 0, e = input.length() - 1;
		
		while(s < e){
			if(input.charAt(s++) != input.charAt(e--))
				return false;
		}
		
		return true;
	}
	
	public static void main(String args[]){
		//System.out.println(validateParanthesis("{{[({}]}}"));
		String input = "abcabaacba";
		System.out.println(isPalindromes(input));
		System.out.println(isPalindrome(input));
	//	System.out.println(getWordCount("the quick fox fox the quick one"));
	}
}
