import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;
import java.util.Stack;


public class StringUtils {

	static String validateString(String value) {
		java.util.Stack<Character> stack = new java.util.Stack<Character>();

		for(int valueIndex = 0; valueIndex < value.length(); valueIndex ++){
			if(value.charAt(valueIndex) == '{' 
					|| value.charAt(valueIndex) == '('
					|| value.charAt(valueIndex) == '[')

				stack.push(value.charAt(valueIndex));
			else if (value.charAt(valueIndex) == '}'
					|| value.charAt(valueIndex) == ')'
					|| value.charAt(valueIndex) == ']'){

				if(stack.isEmpty() || 
						!areMatchingParametersPair(stack.peek(), value.charAt(valueIndex))){
					return "NO";
				} else {
					stack.pop();
				}
			}
		}
		if(stack.isEmpty())
			return "YES";
		else 
			return "NO";


	}


	public static int calculateMinimumCost(){

		LinkedList<HashSet<Integer>> allPrisonnerGroups = new LinkedList<HashSet<Integer>>();

		Scanner in = new Scanner(System.in);
		int numInmates = in.nextInt();
		int pairOfInmates = in.nextInt();
		int cost = 0;

		HashSet<Integer> firstInmateGroup = new HashSet<Integer>();
		firstInmateGroup.add(in.nextInt());
		firstInmateGroup.add(in.nextInt());
		allPrisonnerGroups.add(firstInmateGroup);

		while(pairOfInmates > 1){

			int p1 = in.nextInt();
			int p2 = in.nextInt();

			for(int groupIndex = 0; groupIndex < allPrisonnerGroups.size(); groupIndex ++){
				HashSet<Integer> inmateGroup = allPrisonnerGroups.get(groupIndex);

				if(inmateGroup.contains(p1) || inmateGroup.contains(p2)){
					inmateGroup.add(p1);
					inmateGroup.add(p2);
				} else {
					allPrisonnerGroups.add(new HashSet<Integer>(Arrays.asList(p1, p2)));
				}
			}

			pairOfInmates --;
		}

		int totalInMatesInGroups = 0;
		for(int groupIndex = 0; groupIndex < allPrisonnerGroups.size(); groupIndex++){
			totalInMatesInGroups += allPrisonnerGroups.get(groupIndex).size();
			int tempCost = allPrisonnerGroups.get(groupIndex).size() % 2;
			tempCost = tempCost * tempCost;

			cost += tempCost;
		}

		cost = cost + totalInMatesInGroups - numInmates;

		return cost;


	}


	private static boolean areMatchingParametersPair(Character peek, char charAt) {
		// TODO Auto-generated method stub
		Scanner s;
		return false;
	}

	/**
	 * fails when "aabbcc"
	 * @param input
	 * @return
	 */
	/*public static boolean isPalindrome(String input){
		char xorResult = 0;  
		for(int inputStringIndex = 0; inputStringIndex < input.length(); inputStringIndex++){
			// XOR all the characters in the input string
			xorResult ^= input.charAt(inputStringIndex);
		}

		// If string length is even
		if(input.length() % 2 == 0)
			 if xorResult == 0 then it is a palindrome, as similar characters cancel
	 * out each other 
			return (xorResult == 0);
		else 
			 if length is odd, check if the xorResult equals the middle element in
	 *  the string, if this is a palindrome it will return yes
			return (xorResult == input.charAt(input.length()/2));
	}*/


	public static int evaluatePostfix(String input){
		java.util.Stack<Integer> stack = new java.util.Stack<Integer>();

		for(int i = 0; i < input.length(); i ++){
			char element = input.charAt(i);
			if(Character.isDigit(element)){
				stack.push(Character.getNumericValue(element));
			} else if(isOperator(element)){
				if(stack.size() >= 2) {
					int op2 = stack.pop();
					int op1 = stack.pop();
					stack.push(evaluateExpression(op1, op2, element));
				} else {
					System.out.println("Invalid expression");
					return -1;
				}
			}
		}

		return stack.pop();
	}

	public static int evaluatePrefrix(String input){
		java.util.Stack<Integer> stack = new java.util.Stack<Integer>();
		for(int i = input.length()-1 ; i >= 0; i--){
			char element = input.charAt(i);
			if(Character.isDigit(element)){
				stack.push(Character.getNumericValue(element));
			} else if (isOperator(element)){
				if(stack.size() >= 2) {
					int op1 = stack.pop();
					int op2 = stack.pop();
					stack.push(evaluateExpression(op1, op2, element));
					//System.out.println(stack.peek());
				} else {
					System.out.println("invalid expression");
					return -1;
				}
			}
		}
		return stack.pop();
	}

	private static Integer evaluateExpression(int op1, int op2, char element) {
		switch(element){
		case '^' : return op1 ^ op2;
		case '*' : return op1 * op2;
		case '/' : return op1 / op2;
		case '-' : return op1 - op2;
		case '+' : return op1 + op2;
		default:	System.out.println("Unsupported operation");
		return -10000;
		}
	}


	private static boolean isOperator(char element) {
		return (element == '*' || element == '+' || element == '-' || element == '/' || element == '^');
	}


	public static String infixToPostfix(String input){
		String result = "";
		Stack<Character> stack = new Stack<Character>();

		for(int index = 0; index < input.length(); index ++){
			char ch = input.charAt(index);

			if(Character.isLetter(ch))
				result += ch;
			else if (ch == '(')
				stack.push(ch);
			else if (ch == ')'){
				while(!stack.empty() && stack.peek() != '(')
					result += stack.pop();
				stack.pop();
			} else if(isOperator(ch)){
				while(!stack.empty() && (precedenceOf(ch) <= precedenceOf(stack.peek()))){
					result += stack.pop();
				}
				stack.push(ch);
			}				
		}

		while(!stack.empty())
			result += stack.pop();

		return result;
	}

	private static int precedenceOf(char c){
		switch(c){
		case '^' : return 3;
		case '*' :
		case '/' : return 2;
		case '+' :
		case '-' : return 1;
		default :
			return -100;
		}
	}

	//public static void 

	/*public static boolean isMatching(String input, String pattern){

		int inputIndex = 0, patternIndex = 0;

		while(inputIndex < input.length() && patternIndex < pattern.length()){

			if(input.charAt(inputIndex) == pattern.charAt(patternIndex)){
				inputIndex++; patternIndex++;
				if(patternIndex == pattern.length() - 1){
					return true;
				} 

			} else if (pattern.charAt(patternIndex) == '*'){
				patternIndex ++;
				while(inputIndex < input.length() - 1){
					if(input.charAt(inputIndex) != pattern.charAt(patternIndex)){
						inputIndex ++;
					} else {
						break;
					}
				}
			} else if (pattern.charAt(patternIndex) == '.'){

			} else {
				patternIndex = 0;
			}
		}

		return false;
	}
	 */

	/*public static boolean isMatching(String input, String pattern){

		if(input == null || pattern == null)
			return false;

		if(pattern.length() == 0){
			return input.length() == 0;
		}

		// next char is not '*': must match current character
		if(pattern.charAt(1) != '*'){
			return ((pattern.charAt(0) == input.charAt(0)) || (input.length() != 0))  && isMatching(input.substring(1), pattern.substring(1));
		}

		// next char is '*'
		while((pattern.charAt(0) == input.charAt(0)) || input.length() != 0){
			if(isMatching(input, pattern.substring(2)))
				return true;
			input = input.substring(1);
		}

		return isMatching(input, pattern.substring(2));
	}*/

	public static char findFirstNonRepeatingCharacter(String input){
		LinkedHashMap<Character, Integer> map = new LinkedHashMap<Character, Integer>();

		for(int index = 0; index < input.length(); index ++){
			if(map.containsKey(input.charAt(index))){
				map.put(input.charAt(index), map.get(input.charAt(index))+1);
			} else {
				map.put(input.charAt(index), 1);
			}
		}

		Set<Entry<Character, Integer>> set = map.entrySet();
		for(Entry<Character, Integer> entry : set){
			if(entry.getValue().equals(new Integer(1))){
				return entry.getKey();
			}
		}
		return ' ';
	}

	public boolean checkContainsCharacters(String input, String pattern){

		int patternIndex = 0;
		for(int inputIndex = 0; inputIndex < input.length() && patternIndex < pattern.length();
				inputIndex++){
			if(input.charAt(inputIndex) == pattern.charAt(patternIndex))
				patternIndex++;
		}

		return patternIndex == pattern.length();
	}

	public static boolean checkIfStringHasPalindrome(String input){

		for(int index = 1; index < input.length() ; index++){
			if((input.charAt(index - 1) == input.charAt(index)) 
					|| (input.charAt(index-1) == input.charAt(index + 1))){
				return true;
			}
		}
		return false;
	}

	public static boolean areAnagrams(String s1, String s2){
		if(s1.length() != s2.length())
			return false;

		s1 = s1.toLowerCase();
		s2 = s2.toLowerCase();

		int xorResult = 0;

		// check for spaces or special characters
		for(int index = 0; index < s1.length(); index ++){
			xorResult ^= s1.charAt(index);
		}

		for(int index = 0; index < s2.length(); index++){
			xorResult ^= s2.charAt(index);
		}

		return xorResult == 0;
	}

	public static void printAllPermutations(String input){
		printAllPermutationsHelper("", input);
	}

	private static void printAllPermutationsHelper(String prefix, String input) {
		if(input.length() == 0){
			System.out.println(prefix);
			return;
		}

		for(int i = 0; i < input.length(); i ++){
			printAllPermutationsHelper(prefix + input.charAt(i), 
					input.substring(0, i) + input.substring(i + 1));
		}
	}

	public static void printAllCombinations(String input){
		printAllCombinationsHelper("", input);
	}

	private static void printAllCombinationsHelper(String prefix, String input) {
		System.out.println(prefix);

		if(prefix.length() == input.length()){
			return;
		}

		for(int i = 0; i < input.length(); i ++){
			printAllCombinationsHelper(prefix + input.charAt(i), input);
		}
	}


	public static void main(String args[]){

		//System.out.println(areAnagrams("aaac", "aaac"));

		System.out.println(infixToPostfix("a+b*(c^d-e)^(f+g*h)-i"));
		//	printAllCombinations("abc");
		//printAllPermutations("abc");
		//System.out.println(checkIfStringHasPalindrome("aba"));
		/*char c = 0;
		char a ='a';
		char b = 'b';
		char rsult = (char) (c^a^b^a);
		//System.out.println(rsult);
		System.out.println(isPalindrome("aabbcc"));*/
		//	System.out.println(evaluatePostfix("23*54*+9-"));
		//System.out.println(evaluatePrefrix("-+*23*549"));
		//System.out.println(findFirstNonRepeatingCharacter("geeksforgeeks"));
		//System.out.println(isMatching("aa","a"));
	}
}
